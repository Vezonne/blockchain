#include "declarations.h"

//EXERCICE 3 - MANIPULATION DE STRUCTURES

//Q3.2
void init_key(Key* key, long val, long n){
  key->val = val;
  key->n = n;
}

//Q3.3
void init_pair_keys(Key* pKey, Key* sKey, long low_size, long up_size){
  long p = random_prime_number(low_size, up_size, 5000);
  long q = random_prime_number(low_size, up_size, 5000);
  while(p == q){
    q = random_prime_number(low_size, up_size, 5000);
  }
  generate_key_values(p, q, &pKey->n, &sKey->val, &pKey->val);
  if (pKey->val < 0){
        long t = (p - 1) * (q - 1);
        pKey->val += t; //on aura toujours s*u mod t = 1
    }
  sKey->n = pKey->n;
}

//Q3.4
char* key_to_str(Key* key){
  char *str = malloc(12*sizeof(char));
  if (str == NULL){
    printf("Erreur d'allocation\n");
    return 0;
  }
  sprintf(str, "(%lx,%lx)", key->val, key->n);
  return str;
}

Key* str_to_key(char* str){
  Key* key = malloc(sizeof(Key));
  if(key == NULL){
    printf("Erreur d'allocation\n");
    return 0;
  }
  sscanf(str, "(%lx,%lx)", &key->val, &key->n);
  return key;
}

int equal_key(Key* key1, Key* key2){
  if(key1->val == key2->val && key1->n == key2->n){
    return 1;
  }
  return 0;
}

//SIGNATURE

//Q3.6
Signature* init_signature(long* content, int size){
    Signature* s = malloc(sizeof(Signature));
    if (s == NULL){
      printf("Erreur d'allocation\n");
      return 0;
    }
    s->content = content;
    s->size = size;
    return s;
}

//Q3.7
Signature* sign(char* mess, Key* sKey){
    long* e = encrypt(mess, sKey->val, sKey->n);
    Signature* s =  init_signature(e, strlen(mess));
    return s;
}

//Q3.8
char* signature_to_str(Signature* sgn){
    char* result = malloc(10*sgn->size*sizeof(char));
    if (result == NULL){
      printf("Erreur d'allocation\n");
      return 0;
    }
    result[0]='#';
    int pos = 1;
    char buffer[256];
    for (int i =0; i < sgn->size; i++){
        sprintf (buffer , "%lx", sgn->content[i]) ;
        for (int j =0; j < strlen(buffer); j++) {
            result[pos] = buffer[j];
            pos = pos+1;
        }
        result[pos] = '#';
        pos = pos+1;
    }
    result[pos] = '\0' ;
    result = realloc(result, (pos+1)*sizeof(char));
    if(result == NULL){
      printf("Erreur de reallocation\n");
      return 0;
    }
    return result;
}

Signature* str_to_signature(char* str){
    int len = strlen(str);
    long* content = (long*)malloc(sizeof(long) * len);
    if(content == NULL){
      printf("Erreur d'allocation\n");
      return 0;
    }
    int num = 0;
    char buffer [256];
    int pos = 0;
    for (int i=0; i < len; i++){
        if (str[i]!='#'){
            buffer[pos] = str[i];
            pos = pos+1;
        }
        else {
            if (pos!=0) {
                buffer[pos] = '\0';
                sscanf(buffer, "%lx", &(content[num]));
                num = num+1;
                pos = 0;
            }
        }
    }
    Signature* sgn = init_signature(content, num);
    return sgn;
}

void free_signature(Signature* s){
  free(s->content);
  free(s);
}

//Q3.10
Protected* init_protected(Key* pKey, char* mess, Signature* sgn){
    Protected* pr = (Protected*)malloc(sizeof(Protected));
    if(pr == NULL){
      printf("Erreur d'allocation\n");
      return 0;
    }
    pr->pKey = pKey;
    pr->mess = mess;
    pr->sgn = sgn;
    return pr;
}

//Q3.11
int verify(Protected* pr){
    char* str = decrypt(pr->sgn->content, pr->sgn->size, pr->pKey->val, pr->pKey->n);
    if(str == NULL){
      printf("Erreur d'allocation\n");
      return 0;
    }
    if (strcmp(pr->mess, str)==0){
        free(str);
        return 1;
    }
    free(str);
    return 0;
}

//Q3.12
char* protected_to_str(Protected* pr){
    char* strKey = key_to_str(pr->pKey);
    char* strsgn = signature_to_str(pr->sgn);
    char* str = (char*)malloc(256*sizeof(char));
    if(str == NULL){
      printf("Erreur d'allocation\n");
      free(strKey);
      free(strsgn);
      return 0;
    }
    sprintf(str, "%s %s %s\n", strKey, pr->mess, strsgn);
    str = realloc(str, (strlen(str)+1)*sizeof(char));
    free(strKey);
    free(strsgn);
    return str;
}

Protected* str_to_protected(char* str){
    char strKey[256];
    char strsgn[256];
    char* mess = malloc(256*sizeof(char));
    if(mess == NULL){
      printf("Erreur d'allocation\n");
      return 0;
    }
    sscanf(str,"%s %s %s", strKey, mess, strsgn);
    Key* key = str_to_key(strKey);
    Signature* sgn = str_to_signature(strsgn);
    mess = realloc(mess, (strlen(mess) + 1) * sizeof(char));
    if(mess == NULL){
      free(key);
      free_signature(sgn);
      printf("Erreur de reallocation\n");
      return 0;
    }
    Protected* pr = init_protected(key, mess, sgn);
    return pr;
}

void free_protected(Protected* pr){
  free(pr->pKey);
  free(pr->mess);
  free_signature(pr->sgn);
  free(pr);
}

//EXERCICE 4 - CREATION DE DONNEES POUR SIMULER LE PROCESSUS DE VOTE

//Q4.1
void generate_random_data(int nv, int nc){
    FILE* fk = fopen("Fichiers/keys.txt", "wr");
    FILE* fc = fopen("Fichiers/candidates.txt", "wr");
    FILE* fd = fopen("Fichiers/declarations.txt", "wr");
    if(fk == NULL || fc == NULL || fd == NULL){
       printf("Erreur d'ouverture d'un fichier dans generate_random_data\n");
       exit(0);
    }
    Key** pktab = malloc(nv * sizeof(Key*));
    Key** sktab = malloc(nv * sizeof(Key*));
    Key** ctab = malloc(nc * sizeof(Key*));

    if (pktab == NULL || sktab == NULL || ctab == NULL){
      printf("Erreur d'allocation\n");
      exit(0);
    }

    for (int i = 0; i < nv; i++){
      pktab[i] = (Key*)malloc(sizeof(Key));
      sktab[i] = (Key*)malloc(sizeof(Key));
      Key* pk = (Key*)malloc(sizeof(Key));
      Key* sk = (Key*)malloc(sizeof(Key));
      if(pktab[i] == NULL || sktab[i] == NULL || pk == NULL || sk == NULL){
        printf("Erreur d'allocation de pktab[i] ou sktab[i]\n");
        exit(0);
      }
      init_pair_keys(pk, sk, 3, 7);
      init_key(pktab[i], pk->val, pk->n);
      init_key(sktab[i], sk->val, sk->n);
      char* strpk = key_to_str(pktab[i]);
      char* strsk = key_to_str(sktab[i]);
      fprintf(fk,"%s %s\n", strpk, strsk);
      free(pk);
      free(sk);
      free(strpk);
      free(strsk);
    }

    for (int i = 0; i < nc; i++){
      ctab[i] = (Key*)malloc(sizeof(Key));
      if (ctab[i] == NULL){
        printf("Erreur d'allocation\n");
        exit(0);
      }
      int r = rand()%nv;
      int x = 1;
      while (x != 0){
        for (int j = 0; j < i; j++){
          if (equal_key(ctab[j], pktab[r])){
            r = rand()%nv;
            j = -1;
          }
        }
        x = 0;
      }
      init_key(ctab[i], pktab[r]->val, pktab[r]->n);
      char* strkey = key_to_str(ctab[i]);
      fprintf(fc,"%s\n",strkey);
      free(strkey);
    }

    for (int i = 0; i < nv; i++){
      Key* pk = pktab[i];
      Key* sk = sktab[i];
      int r = rand()%nc;
      char* mess = malloc(256*sizeof(char));
      if(mess == NULL){
        printf("Erreur d'allocation\n");
        exit(0);
      }
      free(mess);
      mess = key_to_str(ctab[r]);
      Signature* sgn = sign(mess, sk);
      Protected* pr = init_protected(pk, mess, sgn);
      char* strpr = protected_to_str(pr);
      fprintf(fd,"%s", strpr);
      free_protected(pr);
      free(strpr);
    }

    for(int i = 0; i < nv; i++){
      free(sktab[i]);
    }

    for(int i = 0; i < nc; i++){
      free(ctab[i]);
    }

    free(pktab);
    free(sktab);
    free(ctab);

    fclose(fk);
    fclose(fc);
    fclose(fd);
}

Key* copy_key(Key* key) {
  Key *copy = malloc(sizeof(Key));
  if(copy == NULL){
    printf("Erreur d'allocation\n");
    return 0;
  }
  copy->n = key->n;
  copy->val = key->val;
  return copy;
}

Signature* copy_signature(Signature* sgn) {
    return init_signature(sgn->content, sgn->size);
}

Protected* copy_protected(Protected* pr) {
    Protected* copy = malloc(sizeof(Protected));
    if(copy == NULL){
      printf("Erreur d'allocation\n");
      return 0;
    }
    copy->pKey = copy_key(pr->pKey);
    copy->sgn = copy_signature(pr->sgn);
    copy->mess = strdup(pr->mess);
    return copy;
}
