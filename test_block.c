#include "block.h"

int main(){
  srand(time(NULL));

  //Testing Blocks
  generate_random_data(10,10);
  Block* block1 = (Block*)malloc(sizeof(Block));
  Key* pkey = (Key*)malloc(sizeof(Key));
  Key* skey = (Key*)malloc(sizeof(Key));
  init_pair_keys(pkey, skey, 3, 7);
  free(skey);
  CellProtected* ld = read_protected("Fichiers/declarations.txt");
  unsigned char* s = (unsigned char*)"Rosetta code";

  block1->author = pkey;
  block1->votes = ld;
  block1->hash = hash_value(s);
  block1->previous_hash = hash_value(s);
  block1->nonce = 0;

  //Testing write_block_file
  printf("\nTesting write block: \n");
  write_block_file(block1, "Fichiers/block.txt");

  //Testing read_block
  printf("\nTesting read block: \n");
  Block* block2 = read_block("Fichiers/block.txt");
  
  //Testing block_to_str
  printf("\nTesting block to str: \n");
  char* strblock = block_to_str(block1);
  printf("%s\n", strblock);
  free(strblock);

  printf("Testing hash_value: \n");
  char* hv = (char*)hash_value(s);
  printf("%s\n", hv);
  free(hv);

  printf("\nTesting compute_proof_of_work: \n");
  free(block1->hash);
  clock_t start1, end1;
  start1 = clock();
  compute_proof_of_work(block1, 3);
  printf("Bloc: %s, nombre de hachages %d\n", (char*)block1->hash, block1->nonce);

  end1 = clock();
  double cpu1 = ((double)(end1 - start1)) / CLOCKS_PER_SEC;
  printf("\nTemps d'execution compute_proof_of_work: %f\n", cpu1);

  clock_t start2, end2;
  FILE* f = fopen("Fichiers/compute_proof_of_work.txt", "w");
  for(int i = 0; i < 5; i++){
    start2 = clock();
    free(block1->hash);
    compute_proof_of_work(block1, i);
    end2 = clock();
    double cpu2 = ((double)(end2 - start2)) / CLOCKS_PER_SEC;
    fprintf(f, "%d %f\n", i, cpu2);
  }
  fclose(f);

  printf("\nTesting verify_block:\n");
  printf("%d\n", verify_block(block1, 3));

  //Testing delete block

  Key* key = block2->author;
  CellProtected* lp = block2->votes;
  delete_block(block1); //check valgrind
  free(pkey);
  delete_list_protected(ld);
  delete_block(block2);
  free(key);
  delete_list_protected(lp);

  return 0;
}
