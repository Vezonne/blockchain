#ifndef BLOCKCHAIN_H
#define BLOCKCHAIN_H
#include "block_tree.h"
#include <dirent.h>

//EXERCICE 9

void submit_vote(Protected* p);

void create_block(char* previous_hash, Key* author, int d);

void add_block(int d, char* name);

CellTree* read_tree(int sizeT);

Key* compute_winner_BT(CellTree* tree, CellKey* candidates, CellKey* voters, int sizeC, int sizeV);

#endif
