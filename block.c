#include "block.h"

//Q7.1
void write_block_file(Block* block, char* file){
  FILE* f = fopen(file, "w");
  if(f == NULL){
       printf("Erreur d'ouverture fichier dans write_block_file\n");
       exit(0);
   }
   char* strkey = key_to_str(block->author);
   fprintf(f, "Author: %s\nBloc: %s\nBloc précedent: %s\nPreuve: %d\n", strkey, (char*)block->hash, (char*)block->previous_hash, block->nonce);
   free(strkey);
   CellProtected* votes = block->votes;
   while(votes){
     char* strpr = protected_to_str(votes->data);
     fprintf(f, "%s", strpr);
     free(strpr);
     votes = votes->next;
   }
   fclose(f);
}

//Q7.2
Block* read_block(char* filename){
  FILE* f = fopen(filename, "r");
  if (f == NULL){
    printf("Erreur d'ouverture d'un fichier dans read_block");
    return 0;
  }
  Block* block = (Block*)malloc(sizeof(Block));
  if (block == NULL){
    printf("Erreur d'allocation dans read_block");
    return 0;
  }
  char line[256];

  fgets(line, 256, f);
  char strkey[12];
  sscanf(line, "Author: %s\n", strkey); //reads author
  block->author = str_to_key(strkey);

  fgets(line, 256, f);
  char hash[256];
  sscanf(line, "Bloc: %s\n", hash); //reads hash
  block->hash = (unsigned char*)strdup(hash);

  fgets(line, 256, f);
  char previous_hash[256];
  sscanf(line, "Bloc précedent: %s\n", previous_hash); //reads previous_hash
  if(strcmp(previous_hash, "(null)") == 0){
    block->previous_hash = NULL;
  }
  else{
    block->previous_hash = (unsigned char*)strdup(previous_hash);
  }
  fgets(line, 256, f);
  int nonce;
  sscanf(line, "Preuve: %d\n", &nonce); //reads proof of work
  block->nonce = nonce;

  fgets(line, 256, f);
  Protected* pr = str_to_protected(line);
  CellProtected* cellProtected = create_cell_protected(pr);

  while(fgets(line, 256, f)){ //reads votes
    Protected* pr = str_to_protected(line);
    cellProtected = add_cell_protected(pr, cellProtected);
  }
  block->votes = cellProtected;
  fclose(f);
  return block;
}

//Q7.3
char* block_to_str(Block* block){
  char* str = malloc(1000 * sizeof(char));
  if(str == NULL){
    printf("Erreur d'allocations dans block_to_str\n");
    return NULL;
  }
  str[0] = '\0';
  char* strkey = key_to_str(block->author);
  char* strCell = malloc(850 * sizeof(char));
  strCell[0] = '\0';
  CellProtected* votes = block->votes;
  while(votes){
    char* strpr = protected_to_str(votes->data);
    strcat(strCell, strpr);
    votes = votes->next;
    free(strpr);
  }
  strCell = realloc(strCell, (strlen(strCell)+1)*sizeof(char));
  sprintf(str, "%s\n%s\n%s\n%d\n", strkey, strCell, (char*)block->previous_hash, block->nonce);
  free(strkey);
  free(strCell);
  str = realloc(str, (strlen(str)+1)*sizeof(char));
  return str;
}

//Q7.5
unsigned char* hash_value(const unsigned char* s){
  unsigned char shaBuffer[SHA256_DIGEST_LENGTH];
  unsigned char* ch = SHA256(s, strlen((char*)s), shaBuffer);
  unsigned char* str = malloc((2*SHA256_DIGEST_LENGTH+1)*sizeof(unsigned char));
  str[0] = '\0';
  char buffer[2*SHA256_DIGEST_LENGTH];
  for (int i = 0; i < SHA256_DIGEST_LENGTH; i++){
    strcpy(buffer, (char*)str);
    sprintf((char*)str, "%s%02x", buffer, ch[i]);
  }
  return str;
}

//Q7.6
void compute_proof_of_work(Block* B, int d){
  int v = 1;
  unsigned char o = (unsigned char)'0';
  char* hash;
  while(v == 1){
    v = 0;
    char* strB = block_to_str(B);
    hash = (char*)hash_value((unsigned char*)strB);
    free(strB);
    for (int i = 0; i < d; i++){
      if (hash[i] != o){
        v = 1;
        break;
      }
    }
    if(v == 1){
      free(hash);
      B->nonce++;
    }
  }
  B->hash = (unsigned char*)hash;
}

//Q7.7
int verify_block(Block* B, int d){
  unsigned char o = (unsigned char)'0';
  int i;
  for(i = 0; i < d; i++){
    if(B->hash[i] != o){
      i = -1;
      break;
    }
  }

  if(i == -1){
    return 0;
  }
  return 1;
}

//Q7.9
void delete_block(Block* b){
  free(b->hash);
  free(b->previous_hash);
  free(b);
}

void free_block(Block* b){
  free(b->hash);
  free(b->previous_hash);
  free(b->author);
  delete_list_protected(b->votes);
  free(b);
}

char* read_hash(char* filename){ //renvoie la valeur hachée du bloc qui se trouve dans un fichier
  FILE* f = fopen(filename, "r");
  if (f == NULL){
    printf("Erreur d'ouverture d'un fichier dans read_hash\n");
    return 0;
  }

  char line[256];
  for(int i = 0; i < 2; i++){
    fgets(line, 256, f);
  }
  char hash[256];
  sscanf(line, "Bloc: %s\n", hash);
  fclose(f);
  return strdup(hash);
}
