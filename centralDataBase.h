#ifndef CENTRALDATABASE_H
#define CENTRALDATABASE_H
#include "declarations.h"

//Structures

//EXERCICE 5
typedef struct cellKey{
  Key* data;
  struct cellKey* next;
} CellKey;

typedef struct cellProtected{
  Protected* data;
  struct cellProtected* next;
} CellProtected;

//EXERCICE 6
typedef struct hashcell{
  Key* key;
  int val;
} HashCell;

typedef struct hashtable{
  HashCell** tab;
  int size;
} HashTable;

//EXERCICE 5
CellKey* create_cell_key(Key* key);

CellKey* add_cell_key(Key* key, CellKey* list);

CellKey* read_public_keys(char* file);

void print_list_keys(CellKey* LCK);

void delete_cell_key(CellKey* c);

void delete_list_key(CellKey* cellKey);

CellProtected* create_cell_protected(Protected* pr);

CellProtected* add_cell_protected(Protected* pr, CellProtected* list);

CellProtected* read_protected(char* file);

void print_list_protected(CellProtected* LCP);

void delete_cell_protected(CellProtected* c);

void delete_list_protected(CellProtected* cellProtected);

//EXERCICE 6
void verify_list_cell(CellProtected** cellProtected);

HashCell* create_hashcell(Key* key);

int hash_function(Key* key, int size);

int find_posotion(HashTable* t, Key* key);

HashTable* create_hashtable(CellKey* keys, int size);

void delete_hashtable(HashTable* t);

Key* compute_winner(CellProtected* decl, CellKey* candidates, CellKey* voters, int sizeC, int sizeV);

#endif
