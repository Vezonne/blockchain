#include "blockchain.h"


int main(){

  srand(time(NULL));

  //suppression de fichiers résidues dans le dossier de la blockchain
  DIR *rep = opendir("./Fichiers/Blockchain/");
  if (rep != NULL){
    struct dirent* dir;
    while ((dir = readdir(rep))) {
      if (strcmp(dir->d_name, ".") != 0 && strcmp(dir->d_name, "..") != 0){
        char name[278];
        sprintf(name, "./Fichiers/Blockchain/%s", dir->d_name);
        remove(name);
      }
    }
    closedir(rep);
  }
  remove("./Fichiers/Pending_block");
  remove("./Fichiers/Pending_votes.txt");

  int sizeC = 5;
  int sizeV = 1000;
  int d = 3;

  printf("Génération des données pour %d citoyens et %d candidats\n", sizeV, sizeC);
  generate_random_data(sizeV, sizeC);

  printf("Lectures des données\n");
  CellProtected* decl = read_protected("Fichiers/declarations.txt");
  CellKey* candidates = read_public_keys("Fichiers/candidates.txt");
  CellKey* voters = read_public_keys("Fichiers/keys.txt");
  CellProtected* vote = decl;
  HashTable* hv = create_hashtable(voters, sizeV);

  printf("Génération de la blockchain avec un hachage à %d zeros\n", d);
  CellTree* tree = NULL;
  char* previous_hash = strdup("(null)");
  int i = 0;
  int j = 0;
  printf("\nSoumission des votes :");
  fflush(stdout);
  while (vote) {
    submit_vote(vote->data);
    vote = vote->next;
    i++;
    if(i == 10 || vote == NULL){
      int r = rand()%sizeV;
      create_block(previous_hash, hv->tab[r]->key, d);
      free(previous_hash);
      char name[10];
      sprintf(name, "block_%d", j);
      add_block(d, name);
      char rename[256];
      sprintf(rename, "Fichiers/Blockchain/%s", name);
      previous_hash = read_hash(rename);
      if(j%2 == 0){
        putchar('#');
        fflush(stdout); //vider le buffer d'affichage
      }
      j++;
      i = 0;
    }
  }
  putchar('\n');
  free(previous_hash);

  printf("\nArbre de la blockchain\n");
  tree = read_tree(sizeV);
  print_tree(tree);

  printf("\nComptage des votes\n");
  Key* wKey = compute_winner_BT(tree, candidates, voters, sizeC, sizeV);
  char* strKey = key_to_str(wKey);
  printf("Gagnant : %s\n", strKey);

  free(strKey);
  free(wKey);
  delete_tree(tree);
  delete_hashtable(hv);
  delete_list_key(voters);
  delete_list_key(candidates);
  delete_list_protected(decl);

  return 0;
}
