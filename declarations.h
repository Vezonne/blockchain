#ifndef DECLARATIONS_H
#define DECLARATIONS_H
#include "encrypt.h"

//Structures

//EXERCICE 3
typedef struct key{
    long val;
    long n;
} Key;

typedef struct sign{
    long *content;
    int size;
} Signature;

typedef struct pr{
    Key* pKey;
    char* mess;
    Signature* sgn;
} Protected;

//EXERCICE 3
void init_key(Key* key, long val, long n);

void init_pair_keys(Key* pKey, Key* sKey, long low_size, long up_size);

char* key_to_str(Key* key);

Key* str_to_key(char* str);

int equal_key(Key* key1, Key* key2);

Signature* init_signature(long* content, int size);

Signature* sign(char* mess, Key* sKey);

char * signature_to_str(Signature* sgn);

Signature* str_to_signature(char* str);

void free_signature(Signature* s);

Protected* init_protected(Key* pKey, char* mess, Signature* sgn);

int verify(Protected* pr);

char* protected_to_str(Protected* pr);

Protected* str_to_protected(char* str);

void free_protected(Protected* pr);

//EXERCICE 4
void generate_random_data(int nv, int nc);

Key* copy_key(Key* key);

Signature* copy_signature(Signature* sgn);

Protected* copy_protected(Protected* pr);

#endif
